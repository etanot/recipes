from calibre.web.feeds.news import BasicNewsRecipe, classes


class Frontline(BasicNewsRecipe):
    # Title to use for the ebook.
    title = "Frontline (Magazine)"

    # A brief description for the ebook.
    description = """India's National Fortnightly Magazine from The Hindu."""

    # Author of this recipe.
    __author__ = "author"

    # Language of the content. Must be an ISO-639 code, either two or three
    # characters long.
    language = "en_IN"

    # Set tags.
    tags = "magazine"

    # Set publisher and publication type.
    publisher = "Frontline"
    publication_type = "magazine"

    # Ask the user for username and password to fetch the content, possible
    # values are: False, "optional", and True. (Default: False)
    needs_subscription = False

    # E-book's masthead (Kindle only) and cover.
    masthead_url = "https://frontline.thehindu.com/favicon.ico"
    cover_url = (
        "https://frontline.thehindu.com/theme/images/fl-online/frontline-logo.png"
    )
    cover_margins = (0, 0, "#000000")

    # The format string for the date shown on the E-book's first page. List of
    # all values: https://docs.python.org/library/time.html. "Default" in
    # "news.py" has a leading space so that's mirrored here. As with "feeds"
    # select/de-select by adding/removing the initial """, only one "timefmt"
    # should be selected, here's a few to choose from:
    #
    # timefmt = " [%a, %d %b %Y]"             # [Fri, 14 Nov 2011] (Default)
    # timefmt = " [%a, %d %b %Y %H:%M]"       # [Fri, 14 Nov 2011 18:30]
    # timefmt = " [%a, %d %b %Y %I:%M %p]"    # [Fri, 14 Nov 2011 06:30 PM]
    # timefmt = " [%d %b %Y]"                 # [14 Nov 2011]
    # timefmt = " [%d %b %Y %H:%M]"           # [14 Nov 2011 18.30]
    # timefmt = " [%Y-%m-%d]"                 # [2011-11-14]
    # timefmt = " [%Y-%m-%d-%H-%M]"           # [2011-11-14-18-30]
    timefmt = " [%a, %d %b, %Y]"

    # Specify an override encoding for sites that have an incorrect charset
    # specified. Default of "None" says to auto-detect, but auto-detecting with
    # "None" isn't also working fine, when article contains some "Hindi" text
    # in it, so enforce encoding to "UTF-8". (To know charset value of the
    # page, look for value of "charset" property in "meta" tag.)
    encoding = "UTF-8"

    # Number of simultaneous downloads. "20" is consistently working fine on
    # the feeds with no problems. Speeds things up from the default of 5. If
    # we have a lot of feeds and/or have increased oldest_article above 2, we
    # may wish to try increasing "simultaneous_downloads" to 25-30, of course,
    # if we are in a hurry. (We've not tried beyond 20.)
    simultaneous_downloads = 20

    # Timeout for fetching files from the server in seconds. The default of
    # 120 seconds, seems somewhat excessive.
    #
    # timeout = 30

    # Disable loading of stylesheets for overly complex stylesheets, unsuitable
    # for conversion to e-book formats. (Default: False)
    no_stylesheets = True

    # Strip all Javascript tags from the downloaded HTML. (Default: True)
    remove_javascript = True

    # Set whether a feed has full articles embedded in it.
    #
    # The Frontline feeds don't have content embedded in it.
    # Possible values: False, None, True. (Default: None)
    use_embedded_content = False

    # Removes empty feeds. (Default: False)
    remove_empty_feeds = True

    # Create a custom title which fits nicely in the Kindle title list.
    # Doesn't require "import time" above the class declaration. Replace
    # "title" with "custom_title" in "conversion_options". Example of string
    # below: "Frontline (Magazine) - 14 Nov".
    custom_title = f"{title} - {time.strftime('%d %b')}"

    """
    # Conversion options for advanced users, to ignore an option, comment it
    # out. Avoid setting "linearize_tables" as that plays havoc with the "old
    # style" table based pages.
    #
    conversion_options = { "title"       : title,
                           "comments"    : description,
                           "tags"        : tags,
                           "language"    : language,
                           "publisher"   : publisher,
                           "authors"     : publisher,
                           "smarten_punctuation" : True,
                         }
    """
    conversion_options = {
        "title": custom_title,
        "comments": description,
        "tags": tags,
        "language": language,
        "publisher": publisher,
        "authors": publisher,
        "base_font_size": 14,
        "smart_punctuation": True,
    }

    # Specify extra CSS - overrides all other CSS (IE added last).
    extra_css = "\
        body {\
          font-family: verdana, helvetica, sans-serif;\
        }\
        .introduction,\
        .first {\
          font-weight: bold;\
        }\
        .cross-head {\
          font-weight: bold;\
          font-size: 125%;\
        }\
        .cap,\
        .caption {\
          display: block;\
          font-size: 80%;\
          font-style: italic;\
        }\
        .cap,\
        .caption,\
        .caption img,\
        .caption span {\
          display: block;\
          text-align: center;\
          margin: 5px auto;\
        }\
        .byl,\
        .byd,\
        .byline img,\
        .byline-name,\
        .byline-title,\
        .author-name,\
        .author-position,\
        .correspondent-portrait img,\
        .byline-lead-in,\
        .name,\
        .role,\
        .bbc-role {\
          display: block;\
          text-align: center;\
          font-size: 80%;\
          font-style: italic;\
          margin: 1px auto;\
        }\
        .story-date,\
        .published,\
        .datestamp {\
          font-size: 80%;\
        }\
        table {\
          width: 100%;\
        }\
        td img {\
          display: block;\
          margin: 5px auto;\
        }\
        ul {\
          padding-top: 10px;\
        }\
        ol {\
          padding-top: 10px;\
        }\
        li {\
          padding-top: 5px;\
          padding-bottom: 5px;\
        }\
        h1 {\
          text-align: center;\
          font-size: 175%;\
          font-weight: bold;\
        }\
        h2 {\
          text-align: center;\
          font-size: 150%;\
          font-weight: bold;\
        }\
        h3 {\
          text-align: center;\
          font-size: 125%;\
          font-weight: bold;\
        }\
        h4,\
        h5,\
        h6 {\
          text-align: center;\
          font-size: 100%;\
          font-weight: bold;\
        }\
    "

    # The maximum age of articles which may be downloaded from each feed. This
    # is specified in days - note fractions of days are allowed, for example,
    # 2.5 (2 and a half days). Setting this to large value will get everything
    # (as far as we can tell) as long as "max_articles_per_feed" remains set
    # high.
    oldest_article = 21

    # The maximum number of articles which may be downloaded from each feed.
    # We've never seen more than about 60 articles in a single feed in the
    # "Frontline" feeds.
    max_articles_per_feed = 100

    # Automatically extract all the text from downloaded article pages.
    # (Default: False)
    auto_cleanup = False

    # Ignore duplicates of articles that are present in more than one section.
    ignore_duplicate_articles = {"title", "url"}

    # Point to downloaded article instead of original web URL. (Default: False)
    resolve_internal_links = True

    # Keep only these tag(s).
    keep_only_tags = [classes("article-section")]

    # List of tags to removed.
    """Remove following classes and text:
    Classes:
        breadcrumb: Remove article's section metadata.
        comments-shares: Remove comment and share icons.
        publish-time: Remove article's publication time

    IDs:
        articleblockcontainer: Remove everything after article's content.
    """
    remove_tags_after = [dict(name="div", id="articleblockcontainer")]
    remove_tags = [
        classes("breadcrumb comments-shares publish-time"),
    ]

    # Remove attributes (can be remove).
    remove_attributes = ["height", "width"]

    #    **** IMPORTANT USERS README ****
    #
    #  First select the feeds we want then scroll down below the feeds list and
    #  select the values we want for the other user preferences, like
    #  oldest_article etc.
    #
    #
    #  Select the Frontline feeds which we want in our ebook. Selected feed
    #  have no "#" at their beginning and de-selected feeds begin with a "#".
    #
    #  Eg.  ("Politics",
    #  "https://frontline.thehindu.com/Politics/feeder/default.rss") - include
    #  feed.
    #  Eg. # ("Politics",
    #  "https://frontline.thehindu.com/Politics/feeder/default.rss") - don't
    #  include feed.
    #
    # There is only few feeds present below which constitute the bulk of the
    # available RSS feeds[1] on Frontline web site.
    #
    # [1]: https://frontline.thehindu.com/rssfeeds/
    #
    # If E-book creation takes too long time, tweak "oldest_article",
    # "max_articles_per_feed", and (especially) "simultaneous_downloads"
    # settings.
    #
    # Select/de-select the feeds we want in our ebook.
    feeds = [
        (
            "Current Issue",
            "https://frontline.thehindu.com/current-issue/feeder/default.rss",
        ),
    ]

    def get_cover_url(self):
        """Get cover page."""
        img_quality = 1200

        soup = self.index_to_soup("https://frontline.thehindu.com/current-issue/")
        tag = soup.find(class_="sptar-image")
        if tag:
            url = tag.find("img")["data-original"]
            url = url.rsplit("/", 2)
            self.cover_url = "/".join([url[0], f"FREE_{img_quality}", url[2]])

        return super().get_cover_url()

    def preprocess_html(self, soup):
        for picture in soup.find_all("picture"):
            source = picture.find(
                "source", srcset=True, attrs={"media": "(min-width: 1600px)"}
            )
            img = picture.find("img", attrs={"src": True})
            if img is None or source is None:
                continue
            img["data-original"] = source["srcset"]

        for img in soup.find_all("img", attrs={"data-original": True}):
            img["src"] = img["data-original"]

        return soup


calibre_most_common_ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"


# vim:fileencoding=utf-8 ft=python

#!/usr/bin/env bash

GIT_DIR=$(basename $(pwd))

pushd ..

# If "custom_recipes" directory doesn't exist, create it.
if [[ ! -d ~/.config/calibre/custom_recipes ]];
then
    mkdir -v ~/.config/calibre/custom_recipes/\
        && echo "Created \"~/.config/calibre/custom_recipes/\" directory."
fi

# Re-links recipes.
stow -Rv "$GIT_DIR" -t ~/.config/calibre/custom_recipes

popd

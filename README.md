# Calibre Recipes

## Supported Sites
  - [Article14](https://article-14.com/)
  - [Alt News](https://www.altnews.in/) [WordPress - Custom Domain]
  - [BBC Hindi](https://bbc.com/hindi/)
  - [Fifty Two (52)](https://fiftytwo.in/)
  - [Financial Times](https://ft.com/)
  - [Frontline (Web)](https://frontline.thehindu.com/)
  - [Frontline (Magazine)](https://frontline.thehindu.com/current-issue/)
  - [Himal Southasian](https://www.himalmag.com/) [WordPress - Custom Domain]
  - [Indian Express](https://indianexpress.com/)
  - [Indian Express (Minimal)](https://indianexpress.com/)
  - [Project Syndicate](https://project-syndicate.org/)
  - [Rest of World](https://restofworld.org/)
  - [The Caravan (Web)](https://caravanmagazine.in/)
  - [The Caravan (Magazine)](https://caravanmagazine.in/magazine/)
  - [The India Cable](https://www.theindiacable.com/)
  - [The India Forum](https://www.theindiaforum.in)
  - [The Juggernaut](https://www.thejuggernaut.com/)
  - [The Leaflet](https://theleaflet.in/) [WordPress - Custom Domain]
  - [The Hindu](https://thehindu.com/)
  - [The Hindu (Minimal)](https://thehindu.com/)
  - [The Wire](https://thewire.in/)


## Installation
Currently installation and update process only supports GNU/Linux operating
systems (OS). Recipes itself OS independent, but requires manually loading of
recipes files from Calibre.

```bash
  $ git clone https://gitlab.com/etanot/recipes.git
  $ cd recipes
  $ ./update.sh
```


## Update
```bash
  $ cd /path/to/recipes/directory/
  $ git fetch origin
  $ ./update.sh
```

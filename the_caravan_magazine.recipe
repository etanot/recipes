import json

from mechanize import Request

from calibre.web.feeds.news import BasicNewsRecipe


def get_author_info(self, slug, byline):
    """Get author's name and its bio."""
    url = f'https://caravanmagazine.in/author/{byline["id"]}'
    author = f'<a href="{url}">{byline["name"]}</a>'

    # Get bio from article's about author's section.
    br = BasicNewsRecipe.get_browser(self)
    article = "".join([self.base_url, slug])
    soup = self.index_to_soup(article)

    bio = soup.find(class_="about-author")
    if bio is None:
        return author, ""

    bio = bio.p
    bio["style"] = "font-size: smaller"
    author_bio = str(bio)

    return author, author_bio


def html_body(sections, body):
    """Convert JSON data to HTML body."""
    for section in sections:
        sec_type = section["type"]
        if sec_type in ["caption", "credit"]:
            if section.get("content") is None:
                continue

            caption = ""
            for text in section["content"]:
                caption = "".join([caption, text["text"]])
            body = "".join(
                [
                    body,
                    "<a>",
                    caption,
                    "</a>",
                ]
            )
        elif sec_type == "embed":
            video = "Watch on "
            attrs = section["attrs"]
            yt_url = f'https://www.youtube.com/watch?v={attrs["id"]}'
            video = "".join(
                [
                    "<p>",
                    video,
                    "Youtube: ",
                    f'<a href="{yt_url}">',
                    attrs["title"],
                    "</a>",
                    "</p>",
                ]
            )
            body = "\n".join([body, video])
        elif sec_type == "figure":
            figure = html_figure(section)
            body = "\n".join([body, figure])
        elif sec_type == "gallery":
            if section.get("content") is None:
                continue

            gallery = html_body(section["content"], "")
            body = "\n".join([body, "<section>", gallery, "</section>"])
        elif sec_type == "paragraph":
            if section.get("content") is None:
                continue

            para = html_para(section)
            body = "\n".join([body, f"<p>{para}</p>"])
        elif sec_type in ["blockquote", "pullquote"]:
            # "pullquote" and "quote" are invalid HTML tags, use "cite" tag for
            # them.
            section_list = section["content"]

            para = ""
            for section in section_list:
                if section.get("content") is None:
                    continue

                para = "\n".join([para, "<p>", html_para(section), "</p>"])
            if sec_type == "pullquote":
                sec_type = "cite"

            body = "\n".join([body, f"<{sec_type}>", para, f"</{sec_type}>"])
        else:
            print(f""""{section['type']}" type undefined, please define it.""")

    return body


def html_figure(section):
    """Convert JSON data to "figure" HTML tag."""
    if section.get("content") is not None:
        caption = html_body(section["content"], "")

    url = optimize_img(section["attrs"]["src"])
    figure = "\n".join(
        [
            "<figure>",
            "<center>",
            f'<img src="{url}">',
            "</center>",
            "<figcaption>",
            caption,
            "</figcaption>",
            "</figure>",
        ]
    )

    return figure


def html_para(section):
    """Convert JSON data to "p" HTML tag."""
    para = ""
    for text in section["content"]:
        mark_b, mark_e = "", ""
        if text.get("text") is None:
            continue

        texts = text["text"]
        if text.get("marks") is not None:
            for mark in text["marks"]:
                tag = mark["type"]
                if tag == "link":
                    url = mark["attrs"]["href"]
                    texts = "".join([f"<a href={url}>", texts, "</a>"])
                else:
                    mark_b = f"<{tag}>"
                    mark_e = f"</{tag}>"
                    texts = "".join([mark_b, texts, mark_e])

        para = "".join([para, texts])

    return para


def optimize_img(url: str, img_quality: int = 750) -> str:
    """Adjust image quality.

    Google CDN contains very high quality (1736 x 2232 pixel) images, so adjust
    image quality according to need.
    """
    return "".join(["https:", url.rsplit("=")[0], f"=s{img_quality}"])


class CaravanMagazine(BasicNewsRecipe):
    # Title to use for the ebook.
    title = "The Caravan (Magazine)"

    # A brief description for the ebook.
    description = "An Indian Journal of politics and culture."

    # Author of this recipe.
    __author__ = "author"

    # Language of the content. Must be an ISO-639 code, either two or three
    # characters long.
    language = "en_IN"

    # Set tags.
    tags = "magazine"

    # Set publisher and publication type.
    publisher = "Delhi Press"
    publication_type = "magazine"

    # Ask the user for username and password to fetch the content, possible
    # values are: False, "optional", and True. (Default: False)
    needs_subscription = "optional"

    # E-book's masthead (Kindle only) and cover.
    masthead_url = (
        "https://storage.googleapis.com/caravan-b/ic/android-icon-192x192.png"
    )
    cover_url = "https://lh3.googleusercontent.com/R5eHH8xZme5Kl_I4QeRJ2Kx9FxOWAhyKz-odglnkAlAo8g71YGap4RrYBuFeGKdLj-1DMYajZxm-7TD-exY-pQmWqHj_EbGy3MDu4VQa=s1500"
    cover_margins = (0, 0, "#000000")

    # The format string for the date shown on the E-book's first page. List of
    # all values: https://docs.python.org/library/time.html. "Default" in
    # "news.py" has a leading space so that's mirrored here. As with "feeds"
    # select/de-select by adding/removing the initial """, only one "timefmt"
    # should be selected, here's a few to choose from:
    #
    # timefmt = " [%a, %d %b %Y]"             # [Fri, 14 Nov 2011] (Default)
    # timefmt = " [%a, %d %b %Y %H:%M]"       # [Fri, 14 Nov 2011 18:30]
    # timefmt = " [%a, %d %b %Y %I:%M %p]"    # [Fri, 14 Nov 2011 06:30 PM]
    # timefmt = " [%d %b %Y]"                 # [14 Nov 2011]
    # timefmt = " [%d %b %Y %H:%M]"           # [14 Nov 2011 18.30]
    # timefmt = " [%Y-%m-%d]"                 # [2011-11-14]
    # timefmt = " [%Y-%m-%d-%H-%M]"           # [2011-11-14-18-30]
    timefmt = " [%a, %d %b, %Y]"

    # Specify an override encoding for sites that have an incorrect charset
    # specified. Default of "None" says to auto-detect. Auto-detecting with
    # "None" is also working fine, so stick with that for robustness. (To know
    # charset value of the page, look for value of "charset" property in "meta"
    # tag.)
    encoding = "utf-8"

    # Number of simultaneous downloads. "20" is consistently working fine.
    # Speeds things up from the default of 5. If we have a lot of feeds and/or
    # have increased oldest_article above 2, we may wish to try increasing
    # "simultaneous_downloads" to 25-30, of course, if we are in a hurry.
    # (We've not tried beyond 20.)
    simultaneous_downloads = 20

    # Timeout for fetching files from the server in seconds. The default of
    # 120 seconds, seems somewhat excessive.
    #
    # timeout = 30

    # Disable loading of stylesheets for overly complex stylesheets, unsuitable
    # for conversion to e-book formats. (Default: False)
    no_stylesheets = True

    # Strip all Javascript tags from the downloaded HTML. (Default: True)
    remove_javascript = True

    # Create a custom title which fits nicely in the Kindle title list.
    # Doesn't require "import time" above the class declaration. Replace
    # "title" with "custom_title" in "conversion_options". Example of string
    # below: "The Caravan (Magazine) - Nov".
    custom_title = f"{title} - {time.strftime('%b')}"

    """
    # Conversion options for advanced users, to ignore an option, comment it
    # out. Avoid setting "linearize_tables" as that plays havoc with the "old
    # style" table based pages.
    #
    conversion_options = { "title"       : title,
                           "comments"    : description,
                           "tags"        : tags,
                           "language"    : language,
                           "publisher"   : publisher,
                           "authors"     : publisher,
                           "smarten_punctuation" : True,
                         }
    """
    conversion_options = {
        "title": custom_title,
        "comments": description,
        "tags": tags,
        "language": language,
        "publisher": publisher,
        "authors": publisher,
        "base_font_size": 14,
        "smart_punctuation": True,
    }

    # Specify extra CSS - overrides all other CSS (IE added last).
    extra_css = "\
        body {\
          font-family: verdana, helvetica, sans-serif;\
        }\
        .introduction,\
        .first {\
          font-weight: bold;\
        }\
        .cross-head {\
          font-weight: bold;\
          font-size: 125%;\
        }\
        .cap,\
        .caption {\
          display: block;\
          font-size: 80%;\
          font-style: italic;\
        }\
        .cap,\
        .caption,\
        .caption img,\
        .caption span {\
          display: block;\
          text-align: center;\
          margin: 5px auto;\
        }\
        .byl,\
        .byd,\
        .byline img,\
        .byline-name,\
        .byline-title,\
        .author-name,\
        .author-position,\
        .correspondent-portrait img,\
        .byline-lead-in,\
        .name,\
        .role,\
        .bbc-role {\
          display: block;\
          text-align: center;\
          font-size: 80%;\
          font-style: italic;\
          margin: 1px auto;\
        }\
        .story-date,\
        .published,\
        .datestamp {\
          font-size: 80%;\
        }\
        table {\
          width: 100%;\
        }\
        td img {\
          display: block;\
          margin: 5px auto;\
        }\
        ul {\
          padding-top: 10px;\
        }\
        ol {\
          padding-top: 10px;\
        }\
        li {\
          padding-top: 5px;\
          padding-bottom: 5px;\
        }\
        h1 {\
          text-align: center;\
          font-size: 175%;\
          font-weight: bold;\
        }\
        h2 {\
          text-align: center;\
          font-size: 150%;\
          font-weight: bold;\
        }\
        h3 {\
          text-align: center;\
          font-size: 125%;\
          font-weight: bold;\
        }\
        h4,\
        h5,\
        h6 {\
          text-align: center;\
          font-size: 100%;\
          font-weight: bold;\
        }\
    "

    # Automatically extract all the text from downloaded article pages.
    # (Default: False)
    auto_cleanup = False

    # Ignore duplicates of articles that are present in more than one section.
    ignore_duplicate_articles = {"title", "url"}

    # Point to downloaded article instead of original web URL. (Default: False)
    resolve_internal_links = True

    base_url = "https://www.caravanmagazine.in"

    def get_browser(self, *args, **kw):
        br = BasicNewsRecipe.get_browser(self, *args, **kw)

        if not self.username or not self.password:
            return br

        data = json.dumps(
            {"email": self.username, "name": "", "password": self.password}
        )

        if not isinstance(data, bytes):
            data = data.encode("utf-8")

        login = "/".join([self.base_url, "api/users/login"])
        rq = Request(
            url=login,
            data=data,
            headers={
                "Accept": "application/json, text/plain, */*",
                "Origin": "https://caravanmagazine.in",
                "Referer": "https://caravanmagazine.in/",
                "Content-type": "application/json;charset=UTF-8",
            },
            method="POST",
        )

        res = br.open(rq).read()
        res = res.decode("utf-8")
        self.log("Login request response: {}".format(res))

        res = json.loads(res)
        if res["code"] != 200 or res["message"] != "Login success":
            raise ValueError("Login failed, check your username and password")

        return br

    def get_cover_url(self):
        """Get cover page."""
        br = BasicNewsRecipe.get_browser(self)
        cover = "/".join([self.base_url, "api/zones/display/latest-issue"])

        rq = Request(cover)
        latest_issue = json.loads(br.open(rq).read())

        url = latest_issue["coverPhoto"]["data"]["url"]
        self.cover_url = optimize_img(url, img_quality=1500)

        return super().get_cover_url()

    def parse_index(self):
        """To parse magazine's TOC."""
        br = BasicNewsRecipe.get_browser(self)
        issue = "/".join([self.base_url, "magazine"])
        soup = self.index_to_soup(issue)

        articles = []
        for i in range(1, 3):
            rq = Request("/".join([self.base_url, f"api/articles/list?page={str(i)}"]))
            articles += json.loads(br.open(rq).read())["rows"]

        article_id = {}
        for article in articles:
            article_id[article["slug"]] = article["id"]

        # Find current issue cover.
        feeds = []
        sections = soup.find(
            class_=lambda x: x and "current-magazine-issue" in x.split()
        ).find(class_=lambda x: x and "sections" in x.split())
        for section in sections.find_all(class_=lambda x: x and "section" in x.split()):
            a = section.find("a")
            section_title = self.tag_to_string(a)
            self.log("\nSection:", section_title)
            articles = []
            for article in section.find_all("article"):
                details = article.find(class_=lambda x: x and "details" in x.split())
                pre = details.find(class_=lambda x: x and "pre-heading" in x.split())
                if pre is not None:
                    pre.extract()
                a = details.find("a")
                slug = a["href"]
                url = "/".join([self.base_url, "api/articles", article_id[slug]])
                title = self.tag_to_string(a)
                desc = self.tag_to_string(details.find("div"))
                self.log("\t", title, url)
                articles.append({"title": title, "description": desc, "url": url})
            if articles:
                feeds.append((section_title, articles))

        return feeds

    def preprocess_raw_html(self, raw, *a):
        """Convert JSON data into HTML."""
        article = json.loads(raw)

        title = article["title"]
        if article.get("printTitle") is not None:
            title = article["printTitle"]

        if article.get("seoTitle") is not None:
            title = article["seoTitle"]

        desc = ""
        if article.get("description") is not None:
            desc = article["description"]

        authors = ""
        authors_bio = ""
        bylines = article.get("authors")
        if bylines is not None:
            for i, byline in enumerate(bylines):
                if i == 0:
                    authors, authors_bio = get_author_info(
                        self, article["slug"], byline
                    )
                    continue

                author_info = get_author_info(self, article["slug"], byline)
                authors = "".join([authors, ", ", author_info[0]])
                authors_bio = "\n".join([authors_bio, author_info[1]])

        body = ""
        post = article["data"].get("content")
        if post is not None:
            body = html_body(post, body)

        html = "\n".join(
            [
                "<html>",
                "<body>",
                "<h1>",
                title,
                "</h1>",
                "<h3>",
                desc,
                "</h3>",
                "<h5>",
                authors,
                "</h5>",
                "<article>",
                body,
                "</article>",
                "</body>",
                "<footer>",
                authors_bio,
                "</footer>",
                "</html>",
            ]
        )

        return html


calibre_most_common_ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"


# vim:fileencoding=utf-8 ft=python

from datetime import datetime, timedelta

from calibre.utils.date import parse_date
from calibre.web.feeds.news import BasicNewsRecipe, classes


class TheIndiaForum(BasicNewsRecipe):
    # Title to use for the ebook.
    title = "The India Forum"

    # A brief description for the ebook.
    description = """A Journal-Magazine on Contemporary Issues."""

    # Author of this recipe.
    __author__ = "author"

    # Language of the content. Must be an ISO-639 code, either two or three
    # characters long.
    language = "en_IN"

    # Set tags.
    tags = "journal, magazine"

    # Set publisher and publication type.
    publication_type = "journal"
    publisher = "The India Forum"

    # Ask the user for username and password to fetch the content, possible
    # values are: False, "optional", and True. (Default: False)
    needs_subscription = False

    # E-book's masthead (Kindle only) and cover.
    masthead_url = (
        "https://www.theindiaforum.in/themes/the_india_forum/images/quote.png"
    )
    cover_url = (
        "https://www.theindiaforum.in/themes/the_india_forum/images/tif_logo.png"
    )
    cover_margins = (0, 0, "#000000")

    # The format string for the date shown on the E-book's first page. List of
    # all values: https://docs.python.org/library/time.html. "Default" in
    # "news.py" has a leading space so that's mirrored here. As with "feeds"
    # select/de-select by adding/removing the initial """, only one "timefmt"
    # should be selected, here's a few to choose from:
    #
    # timefmt = " [%a, %d %b %Y]"             # [Fri, 14 Nov 2011] (Default)
    # timefmt = " [%a, %d %b %Y %H:%M]"       # [Fri, 14 Nov 2011 18:30]
    # timefmt = " [%a, %d %b %Y %I:%M %p]"    # [Fri, 14 Nov 2011 06:30 PM]
    # timefmt = " [%d %b %Y]"                 # [14 Nov 2011]
    # timefmt = " [%d %b %Y %H:%M]"           # [14 Nov 2011 18.30]
    # timefmt = " [%Y-%m-%d]"                 # [2011-11-14]
    # timefmt = " [%Y-%m-%d-%H-%M]"           # [2011-11-14-18-30]
    timefmt = " [%a, %d %b, %Y]"

    # Specify an override encoding for sites that have an incorrect charset
    # specified. Default of "None" says to auto-detect, but auto-detecting with
    # "None" isn't also working fine, when article contains some "Hindi" text
    # in it, so enforce encoding to "UTF-8". (To know charset value of the
    # page, look for value of "charset" property in "meta" tag.)
    encoding = "UTF-8"

    # Number of simultaneous downloads. "20" is consistently working fine on
    # the feeds with no problems. Speeds things up from the default of 5. If
    # we have a lot of feeds and/or have increased oldest_article above 2, we
    # may wish to try increasing "simultaneous_downloads" to 25-30, of course,
    # if we are in a hurry. (We've not tried beyond 20.)
    simultaneous_downloads = 20

    # Timeout for fetching files from the server in seconds. The default of
    # 120 seconds, seems somewhat excessive.
    #
    # timeout = 30

    # Disable loading of stylesheets for overly complex stylesheets, unsuitable
    # for conversion to e-book formats. (Default: False)
    no_stylesheets = True

    # Strip all Javascript tags from the downloaded HTML. (Default: True)
    remove_javascript = True

    # Set whether a feed has full articles embedded in it.
    #
    # The India Forum feeds don't have content embedded in it.
    # Possible values: False, None, True. (Default: None)
    use_embedded_content = False

    # Removes empty feeds. (Default: False)
    remove_empty_feeds = True

    # Create a custom title which fits nicely in the Kindle title list.
    # Doesn't require "import time" above the class declaration. Replace
    # "title" with "custom_title" in "conversion_options". Example of string
    # below: "The India Forum - 14 Nov 2011".
    custom_title = f"{title} - {time.strftime('%d %b %Y')}"

    """
    # Conversion options for advanced users, to ignore an option, comment it
    # out. Avoid setting "linearize_tables" as that plays havoc with the "old
    # style" table based pages.
    #
    conversion_options = { "title"       : title,
                           "comments"    : description,
                           "tags"        : tags,
                           "language"    : language,
                           "publisher"   : publisher,
                           "authors"     : publisher,
                           "smarten_punctuation" : True,
                         }
    """
    conversion_options = {
        "title": custom_title,
        "comments": description,
        "tags": tags,
        "language": language,
        "publisher": publisher,
        "authors": publisher,
        "base_font_size": 14,
        "smart_punctuation": True,
    }

    # Specify extra CSS - overrides all other CSS (IE added last).
    extra_css = "\
        body {\
          font-family: verdana, helvetica, sans-serif;\
        }\
        .introduction,\
        .first {\
          font-weight: bold;\
        }\
        .cross-head {\
          font-weight: bold;\
          font-size: 125%;\
        }\
        .cap,\
        .caption {\
          display: block;\
          font-size: 80%;\
          font-style: italic;\
        }\
        .cap,\
        .caption,\
        .caption img,\
        .caption span {\
          display: block;\
          text-align: center;\
          margin: 5px auto;\
        }\
        .byl,\
        .byd,\
        .byline img,\
        .byline-name,\
        .byline-title,\
        .author-name,\
        .author-position,\
        .correspondent-portrait img,\
        .byline-lead-in,\
        .name,\
        .role,\
        .bbc-role {\
          display: block;\
          text-align: center;\
          font-size: 80%;\
          font-style: italic;\
          margin: 1px auto;\
        }\
        .story-date,\
        .published,\
        .datestamp {\
          font-size: 80%;\
        }\
        table {\
          width: 100%;\
        }\
        td img {\
          display: block;\
          margin: 5px auto;\
        }\
        ul {\
          padding-top: 10px;\
        }\
        ol {\
          padding-top: 10px;\
        }\
        li {\
          padding-top: 5px;\
          padding-bottom: 5px;\
        }\
        h1 {\
          text-align: center;\
          font-size: 175%;\
          font-weight: bold;\
        }\
        h2 {\
          text-align: center;\
          font-size: 150%;\
          font-weight: bold;\
        }\
        h3 {\
          text-align: center;\
          font-size: 125%;\
          font-weight: bold;\
        }\
        h4,\
        h5,\
        h6 {\
          text-align: center;\
          font-size: 100%;\
          font-weight: bold;\
        }\
    "

    # The maximum age of articles which may be downloaded from each feed. This
    # is specified in days - note fractions of days are allowed, for example,
    # 2.5 (2 and a half days). Our default value 1.5 days is the last 36 hours,
    # the point at which we've decided "news" becomes "old news", but be warned
    # this is not so good for the "blogs", "technology", "magazine", and
    # "sports" feeds. We could extend this to 2-5, but watch out ebook creation
    # time will increase as well. Setting this to large value will get
    # everything (as far as we can tell) as long as "max_articles_per_feed"
    # remains set high.
    oldest_article = 7.5

    # The maximum number of articles which may be downloaded from each feed.
    # We've never seen more than about 20 articles in a single feed in the "The
    # India Forum" feeds.
    max_articles_per_feed = 50

    # Automatically extract all the text from downloaded article pages.
    # (Default: False)
    auto_cleanup = False

    # Ignore duplicates of articles that are present in more than one section.
    ignore_duplicate_articles = {"title", "url"}

    # Point to downloaded article instead of original web URL. (Default: False)
    resolve_internal_links = True

    # Keep only these tag(s).
    keep_only_tags = classes("node__content")

    # List of tags to removed.
    """Remove following classes and text:
    Classes:
        block-article-tiffin: Remove categories like "Forum", "Tiffin" etc.
        block-field-blocknodearticlebody: Remove everthing after article's
                                          body, including last updated date,
                                          tags, "READ ALSO" etc.
        layout__region--second: Remove social media share buttons along with
                                "Listen to this article" player.
        text-right: Remove buttons (like "READ ALSO", "DONATE", "UPDATES" etc.)
                    present right hand side of the article.

    Tags:
        section: Remove duplicate article's metadata like authors and
                 publication date.
    """
    remove_tags = [
        classes("block-article-tiffin layout__region--second text-right"),
        dict(name="section", id="ipad-article-author-top-container"),
    ]
    remove_tags_after = [
        classes("block-field-blocknodearticlebody"),
    ]

    def parse_index(self):
        """Parse Sitemap.xml"""
        base_url = "https://www.theindiaforum.in"

        today = datetime.now()
        issue = today.strftime("%b%Y").lower()
        feed_url = "/".join([base_url, "xmlsm", f"{issue}.xml"])
        soup = self.index_to_soup(feed_url)

        # Get list of all articles.
        cnt = 0
        feeds = []
        sections = dict()
        for article in soup.find_all("url"):
            url = self.tag_to_string(article.loc)
            # Since there is no title present in feed, so recreate it from
            # article's URL.
            title = url.rpartition("/")[2].replace("-", " ").title()
            date = parse_date(self.tag_to_string(article.lastmod)).replace(tzinfo=None)

            # When first old article is found, don't check for other articles.
            # Articles are arranged such a way that they are already sorted
            # with respect to their publication date.
            if (today - date) > timedelta(self.oldest_article):
                self.log(
                    f"""Skipping all articles after, including "{title}" ({date}), as they are likely too old..."""
                )
                break

            # Article's URL is in
            # "https://theindiforum.in</section>/article-name" form. If URL
            # doesn't have "section", assume it to be "Recents", otherwise
            # extract "section" name and put the articles in their respective
            # section.
            section = "Recents"
            slices = url.split("/")
            if len(slices) > 4:
                section = slices[3].capitalize()

            metadata = {
                "title": title,
                "date": date,
                "url": url,
            }

            if section not in sections:
                sections[section] = list()

            sections[section].append(metadata)

            cnt += 1
            if cnt == self.max_articles_per_feed:
                self.log(
                    f"Maximum number of article limit has been reached, i.e. {self.max_articles_per_feed}."
                )
                break

        for section, articles in sections.items():
            self.log("\nSection:", section)
            for article in articles:
                self.log("\t", article["title"])

            feeds.append((section, articles))

        return feeds

    def preprocess_html(self, soup):
        """Fetch cover photo.

        Except "cover photo", all article's photo is being fetched. This
        happens because cover photo is present inside "picture" tag. If we see
        raw HTML document, cover photo is presented as follows:

          <picture>
            <source
              srcset="
                /sites/default/files/styles/cover_story/public/field/image/2022/09/22/zaheer-1663813829.jpg?h=2aaa6797 1x
              "
              media="screen and (min-width: 801px)"
              type="image/jpeg"
            />
            <source
              srcset="
                /sites/default/files/styles/cover_story_medium/public/field/image/2022/09/22/zaheer-1663813829.jpg?h=2aaa6797 1x
              "
              media="screen and (min-width: 601px) and (max-width: 800px)"
              type="image/jpeg"
            />
            <source
              srcset="
                /sites/default/files/styles/cover_story_small/public/field/image/2022/09/22/zaheer-1663813829.jpg?h=2aaa6797 1x
              "
              type="image/jpeg"
            />
            <img
              src="/sites/default/files/styles/cover_story/public/field/image/2022/09/22/zaheer-1663813829.jpg?h=2aaa6797"
              alt=""
              typeof="foaf:Image"
            />
          </picture>

        So create a new "img" tag with "src" value as first "source" tag's
        "srcset" value and replace it with "picture" tag.
        """
        cover = soup.find("picture")
        content = self.tag_to_string(cover.source["srcset"])
        url = content.partition("?")[0]

        img_tag = soup.new_tag("img", src=url)
        cover.insert_before(img_tag)
        cover.decompose()

        # Remove author's pic and if there are multiple authors, make them
        # comma separated instead of newline.
        div_tag = soup.new_tag("div", attrs={"class": "author-name"})
        div_tag.append(soup.new_tag("br"))
        for author in soup.find_all("div", class_="author-item"):
            if len(div_tag.contents) > 1:
                div_tag.append(", ")

            div_tag.append(author.h4.a)

        parent_tag = author.parent
        parent_tag.insert_before(div_tag)
        parent_tag.decompose()

        return soup


calibre_most_common_ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"


# vim:fileencoding=utf-8 ft=python

from calibre.web.feeds.news import BasicNewsRecipe


class BBCHindi(BasicNewsRecipe):
    # Title to use for the ebook.
    title = "BBC Hindi"

    # A brief description for the ebook.
    description = "BBC Hindi News"

    # Author of this recipe.
    __author__ = "author"

    # Language of the content. Must be an ISO-639 code, either two or three
    # characters long.
    language = "hi"

    # Set tags.
    tags = "news, sport, blog"

    # Set publisher and publication type.
    publication_type = "newspaper"
    publisher = "BBC"

    # Ask the user for username and password to fetch the content, possible
    # values are: False, "optional", and True. (Default: False)
    needs_subscription = False

    # E-book's masthead (Kindle only) and cover.
    masthead_url = "https://static.files.bbci.co.uk/ws/simorgh-assets/public/hindi/images/icons/icon-512x512.png"
    cover_url = "https://news.files.bbci.co.uk/ws/img/logos/og/hindi.png"
    cover_margins = (0, 0, "#000000")

    # The format string for the date shown on the E-book's first page. List of
    # all values: https://docs.python.org/library/time.html. "Default" in
    # "news.py" has a leading space so that's mirrored here. As with "feeds"
    # select/de-select by adding/removing the initial """, only one "timefmt"
    # should be selected, here's a few to choose from:
    #
    # timefmt = " [%a, %d %b %Y]"             # [Fri, 14 Nov 2011] (Default)
    # timefmt = " [%a, %d %b %Y %H:%M]"       # [Fri, 14 Nov 2011 18:30]
    # timefmt = " [%a, %d %b %Y %I:%M %p]"    # [Fri, 14 Nov 2011 06:30 PM]
    # timefmt = " [%d %b %Y]"                 # [14 Nov 2011]
    # timefmt = " [%d %b %Y %H:%M]"           # [14 Nov 2011 18.30]
    # timefmt = " [%Y-%m-%d]"                 # [2011-11-14]
    # timefmt = " [%Y-%m-%d-%H-%M]"           # [2011-11-14-18-30]
    timefmt = " [%a, %d %b, %Y]"

    # Specify an override encoding for sites that have an incorrect charset
    # specified. Default of "None" says to auto-detect. Some other BBC recipes
    # use "utf8", which works fine (so use that if necessary), but
    # auto-detecting with "None" is also working fine, so stick with that for
    # robustness. (To know charset value of the page, look for value of
    # "charset" property in "meta" tag.)
    encoding = None

    # Number of simultaneous downloads. "20" is consistently working fine on
    # the BBC News feeds with no problems. Speeds things up from the default of
    # 5.  If we have a lot of feeds and/or have increased oldest_article above
    # 2, we may wish to try increasing "simultaneous_downloads" to 25-30, of
    # course, if we are in a hurry. (We've not tried beyond 20.)
    simultaneous_downloads = 20

    # Timeout for fetching files from the server in seconds. The default of
    # 120 seconds, seems somewhat excessive.
    #
    # timeout = 30

    # Disable loading of stylesheets for overly complex stylesheets, unsuitable
    # for conversion to e-book formats. (Default: False)
    no_stylesheets = True

    # Strip all Javascript tags from the downloaded HTML. (Default: True)
    remove_javascript = True

    # Set whether a feed has full articles embedded in it. The BBC feeds do
    # not have. Possible values: False, None, True. (Default: None)
    use_embedded_content = False

    # Removes empty feeds. (Default: False)
    remove_empty_feeds = True

    # Create a custom title which fits nicely in the Kindle title list.
    # Doesn't require "import time" above the class declaration. Replace
    # "title" with "custom_title" in "conversion_options". Example of string
    # below: "BBC News - 14 Nov 2011".
    custom_title = f"{title} - {time.strftime('%d %b %Y')}"

    """
    # Conversion options for advanced users, to ignore an option, comment it
    # out. Avoid setting "linearize_tables" as that plays havoc with the "old
    # style" table based pages.
    #
    conversion_options = { "title"       : title,
                           "comments"    : description,
                           "tags"        : tags,
                           "language"    : language,
                           "publisher"   : publisher,
                           "authors"     : publisher,
                           "smarten_punctuation" : True,
                         }
    """
    conversion_options = {
        "title": custom_title,
        "comments": description,
        "tags": tags,
        "language": language,
        "publisher": publisher,
        "authors": publisher,
        "base_font_size": 14,
        "smart_punctuation": True,
    }

    # Specify extra CSS - overrides all other CSS (IE added last).
    extra_css = "\
        body {\
          font-family: verdana, helvetica, sans-serif;\
        }\
        .introduction,\
        .first {\
          font-weight: bold;\
        }\
        .cross-head {\
          font-weight: bold;\
          font-size: 125%;\
        }\
        .cap,\
        .caption {\
          display: block;\
          font-size: 80%;\
          font-style: italic;\
        }\
        .cap,\
        .caption,\
        .caption img,\
        .caption span {\
          display: block;\
          text-align: center;\
          margin: 5px auto;\
        }\
        .byl,\
        .byd,\
        .byline img,\
        .byline-name,\
        .byline-title,\
        .author-name,\
        .author-position,\
        .correspondent-portrait img,\
        .byline-lead-in,\
        .name,\
        .role,\
        .bbc-role {\
          display: block;\
          text-align: center;\
          font-size: 80%;\
          font-style: italic;\
          margin: 1px auto;\
        }\
        .story-date,\
        .published,\
        .datestamp {\
          font-size: 80%;\
        }\
        table {\
          width: 100%;\
        }\
        td img {\
          display: block;\
          margin: 5px auto;\
        }\
        ul {\
          padding-top: 10px;\
        }\
        ol {\
          padding-top: 10px;\
        }\
        li {\
          padding-top: 5px;\
          padding-bottom: 5px;\
        }\
        h1 {\
          text-align: center;\
          font-size: 175%;\
          font-weight: bold;\
        }\
        h2 {\
          text-align: center;\
          font-size: 150%;\
          font-weight: bold;\
        }\
        h3 {\
          text-align: center;\
          font-size: 125%;\
          font-weight: bold;\
        }\
        h4,\
        h5,\
        h6 {\
          text-align: center;\
          font-size: 100%;\
          font-weight: bold;\
        }\
    "

    # The max age of articles which may be downloaded from each feed. This is
    # specified in days - note fractions of days are allowed, for example, 2.5
    # (2 and a half days). Our default value 1.5 days is the last 36 hours, the
    # point at which we've decided "news" becomes "old news", but be warned
    # this is not so good for the "blogs", "technology", "magazine", and
    # "sports" feeds. We may wish to extend this to 2-5, but watch out ebook
    # creation time will increase as well. Setting this to 30 will get
    # everything (as far as we can tell) as long as "max_articles_per_feed"
    # remains set high.
    oldest_article = 1.5

    # The max number of articles which may be downloaded from each feed. We've
    # never seen more than about 70 articles in a single feed in the BBC feeds.
    max_articles_per_feed = 100

    # Automatically extract all the text from downloaded article pages.
    # (Default: False)
    auto_cleanup = False

    # Ignore duplicates of articles that are present in more than one section.
    ignore_duplicate_articles = {"title", "url"}

    # Point to downloaded article instead of original web URL. (Default: False)
    resolve_internal_links = True

    # Keep only these tag(s).
    keep_only_tags = dict(name="main")

    # List of tags to be removed.
    """Remove following and tags:
    Tags:
        h2: Remove "ये-भी-पढ़ें" section along with all tags after it.
        section: Remove region specific contents like "advertisment",
                 "recommended reading", "podcast promo" etc.
        time: Remove article's publication date and time.
    """
    remove_tags = [
        dict(name="h2", attrs={"id": "ये-भी-पढ़ें"}),
        dict(name="section", attrs={"role": "region"}),
        dict(name="time"),
    ]
    remove_tags_after = dict(name="h2", attrs={"id": "ये-भी-पढ़ें"})

    # List of attributes to be removed.
    remove_attributes = ["height", "width"]

    #    **** IMPORTANT USERS README ****
    #
    #  First select the feeds we want then scroll down below the feeds list and
    #  select the values we want for the other user preferences, like
    #  oldest_article etc.
    #
    #
    #  Select the BBC RSS feeds which we want in our ebook. Selected feed have
    #  no "#" at their beginning and de-selected feeds begin with a "#".
    #
    #  Eg.  ("News Home", "https://feeds.bbci.co.uk/..." - include feed.
    #  Eg. # ("News Home", "https://feeds.bbci.co.uk/..." - don't include feed.
    #
    # There is only feed present below which constitute the bulk of the
    # available RSS feeds[1] on the BBC web site. Some old feeds, whose
    # articles not being updated since very long time:
    #
    # ("भारत", "https://www.bbc.co.uk/hindi/india/index.xml"): 25th Sep '14
    # ("पाकिस्तान", "https://www.bbc.co.uk/hindi/pakistan/index.xml"): 26th Sep '14
    # ("चीन", "https://www.bbc.co.uk/hindi/china/index.xml"): 26th Sep '14
    # ("खेल", "https://www.bbc.co.uk/hindi/sport/index.xml"): 25th Sep '14
    # ("मनोरंजन", "https://www.bbc.co.uk/hindi/entertainment/index.xml"): 25th Sep '14
    # ("कारोबार", "https://www.bbc.co.uk/hindi/business/index.xml"): 22nd Sep '12
    # ("विज्ञान", "https://www.bbc.co.uk/hindi/science/index.xml"): 25th Sep '14
    # ("मल्टीमीडियबीबीसी विशेष", "https://www.bbc.co.uk/hindi/indepth/index.xml"): 18th Sep '14
    # ("स्पेशल", "https://feeds.bbci.co.uk/hindi/indepth/rss.xml"): Archived since 22nd Sep '14
    #
    # [1]: https://www.bbc.com/hindi/institutional/2011/09/000001_rss
    #
    # Except few feeds like "Pakistan", "China", and "Business", all other
    # feeds are available via "feeds.bbci.co.uk" URL.
    #
    # If E-book creation takes too long time, tweak "oldest_article",
    # "max_articles_per_feed", and (especially) "simultaneous_downloads"
    # settings.
    #
    # Select/de-select the feeds we want in our ebook.
    feeds = [
        ("भारत", "https://feeds.bbci.co.uk/hindi/india/rss.xml"),
        ("अंतरराष्ट्रीय", "https://feeds.bbci.co.uk/hindi/international/rss.xml"),
        # ("पाकिस्तान", "https://www.bbc.co.uk/hindi/pakistan/index.xml"),
        # ("चीन", "https://www.bbc.co.uk/hindi/china/index.xml"),
        # ("कारोबार", "https://www.bbc.co.uk/hindi/business/index.xml"),
        ("विज्ञान - टेक्नॉलॉजी", "https://feeds.bbci.co.uk/hindi/science/rss.xml"),
        ("मैगज़ीन", "https://feeds.bbci.co.uk/hindi/magazine/rss.xml"),
        ("सोशल", "https://feeds.bbci.co.uk/hindi/social/rss.xml"),
        ("खेल", "https://feeds.bbci.co.uk/hindi/sport/rss.xml"),
        ("मनोरंजन", "https://feeds.bbci.co.uk/hindi/entertainment/rss.xml"),
        ("पहला पन्ना", "https://feeds.bbci.co.uk/hindi/rss.xml"),
    ]

    def get_article_url(self, article):
        """Remove RSS campagin tracking parameters."""
        return article.link.partition("?")[0]

    def preprocess_html(self, soup):
        """Fetch all photos present in the article.

        Except article's cover photo, no other photo is being fetched. This
        happens because all "img" tag is present between "noscript" tag, except
        cover photo. So remove all "noscript" tags while keeping its content.
        """
        for tag in soup.find_all("noscript"):
            tag.unwrap()

        return soup


calibre_most_common_ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"


# vim:fileencoding=utf-8 ft=python

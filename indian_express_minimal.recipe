from datetime import datetime, timedelta
import json
import re

from calibre.utils.date import parse_date
from calibre.web.feeds.news import BasicNewsRecipe, classes


class IndianExpress(BasicNewsRecipe):
    # Title to use for the ebook.
    title = "Indian Express (Minimal)"

    # A brief description for the ebook.
    description = """Today's news: Get latest and Breaking News on Politics,
    Business, Lifestyle, Entertainment and Sports along with News updates from
    around the world."""

    # Author of this recipe.
    __author__ = "author"

    # Language of the content. Must be an ISO-639 code, either two or three
    # characters long.
    language = "en_IN"

    # Set tags.
    tags = "news"

    # Set publisher and publication type.
    publication_type = "news"
    publisher = "Indian Express"

    # Ask the user for username and password to fetch the content, possible
    # values are: False, "optional", and True. (Default: False)
    needs_subscription = False

    # E-book's masthead (Kindle only) and cover.
    masthead_url = "https://images.indianexpress.com/2018/10/fav-icon.png"
    cover_url = (
        # "https://indianexpress.com/wp-content/themes/indianexpress/images/IE-OGimage.jpg"
        "https://indianexpress.com/wp-content/themes/indianexpress/images/indian-express-logo.svg"
    )
    cover_margins = (0, 0, "#000000")

    # The format string for the date shown on the E-book's first page. List of
    # all values: https://docs.python.org/library/time.html. "Default" in
    # "news.py" has a leading space so that's mirrored here. As with "feeds"
    # select/de-select by adding/removing the initial """, only one "timefmt"
    # should be selected, here's a few to choose from:
    #
    # timefmt = " [%a, %d %b %Y]"             # [Fri, 14 Nov 2011] (Default)
    # timefmt = " [%a, %d %b %Y %H:%M]"       # [Fri, 14 Nov 2011 18:30]
    # timefmt = " [%a, %d %b %Y %I:%M %p]"    # [Fri, 14 Nov 2011 06:30 PM]
    # timefmt = " [%d %b %Y]"                 # [14 Nov 2011]
    # timefmt = " [%d %b %Y %H:%M]"           # [14 Nov 2011 18.30]
    # timefmt = " [%Y-%m-%d]"                 # [2011-11-14]
    # timefmt = " [%Y-%m-%d-%H-%M]"           # [2011-11-14-18-30]
    timefmt = " [%a, %d %b, %Y]"

    # Specify an override encoding for sites that have an incorrect charset
    # specified. Default of "None" says to auto-detect, but auto-detecting with
    # "None" isn't also working fine, when article contains some "Hindi" text
    # in it, so enforce encoding to "UTF-8". (To know charset value of the
    # page, look for value of "charset" property in "meta" tag.)
    encoding = "UTF-8"

    # Number of simultaneous downloads. "20" is consistently working fine on
    # the feeds with no problems. Speeds things up from the default of 5. If
    # we have a lot of feeds and/or have increased oldest_article above 2, we
    # may wish to try increasing "simultaneous_downloads" to 25-30, of course,
    # if we are in a hurry. (We've not tried beyond 20.)
    simultaneous_downloads = 20

    # Timeout for fetching files from the server in seconds. The default of
    # 120 seconds, seems somewhat excessive.
    #
    # timeout = 30

    # Disable loading of stylesheets for overly complex stylesheets, unsuitable
    # for conversion to e-book formats. (Default: False)
    no_stylesheets = True

    # Strip all Javascript tags from the downloaded HTML. (Default: True)
    remove_javascript = True

    # Set whether a feed has full articles embedded in it.
    use_embedded_content = False

    # Removes empty feeds. (Default: False)
    remove_empty_feeds = True

    # Create a custom title which fits nicely in the Kindle title list.
    # Doesn't require "import time" above the class declaration. Replace
    # "title" with "custom_title" in "conversion_options". Example of string
    # below: "Indian Express - 14 Nov 2011".
    custom_title = f"{title} - {time.strftime('%d %b %Y')}"

    """
    # Conversion options for advanced users, to ignore an option, comment it
    # out. Avoid setting "linearize_tables" as that plays havoc with the "old
    # style" table based pages.
    #
    conversion_options = { "title"       : title,
                           "comments"    : description,
                           "tags"        : tags,
                           "language"    : language,
                           "publisher"   : publisher,
                           "authors"     : publisher,
                           "smarten_punctuation" : True,
                         }
    """
    conversion_options = {
        "title": custom_title,
        "comments": description,
        "tags": tags,
        "language": language,
        "publisher": publisher,
        "authors": publisher,
        "base_font_size": 14,
        "smart_punctuation": True,
    }

    # Specify extra CSS - overrides all other CSS (IE added last).
    extra_css = "\
        body {\
          font-family: verdana, helvetica, sans-serif;\
        }\
        .introduction,\
        .first {\
          font-weight: bold;\
        }\
        .cross-head {\
          font-weight: bold;\
          font-size: 125%;\
        }\
        .cap,\
        .caption {\
          display: block;\
          font-size: 80%;\
          font-style: italic;\
        }\
        .cap,\
        .caption,\
        .caption img,\
        .caption span {\
          display: block;\
          text-align: center;\
          margin: 5px auto;\
        }\
        .byl,\
        .byd,\
        .byline img,\
        .byline-name,\
        .byline-title,\
        .author-name,\
        .author-position,\
        .correspondent-portrait img,\
        .byline-lead-in,\
        .name,\
        .role,\
        .bbc-role {\
          display: block;\
          text-align: center;\
          font-size: 80%;\
          font-style: italic;\
          margin: 1px auto;\
        }\
        .story-date,\
        .published,\
        .datestamp {\
          font-size: 80%;\
        }\
        table {\
          width: 100%;\
        }\
        td img {\
          display: block;\
          margin-left: auto;\
          margin-right: auto;\
        }\
        ul {\
          padding-top: 10px;\
        }\
        ol {\
          padding-top: 10px;\
        }\
        li {\
          padding-top: 5px;\
          padding-bottom: 5px;\
        }\
        h1 {\
          text-align: center;\
          font-size: 175%;\
          font-weight: bold;\
        }\
        h2 {\
          text-align: center;\
          font-size: 150%;\
          font-weight: bold;\
        }\
        h3 {\
          text-align: center;\
          font-size: 125%;\
          font-weight: bold;\
        }\
        h4,\
        h5,\
        h6 {\
          text-align: center;\
          font-size: 100%;\
          font-weight: bold;\
        }\
    "

    # The maximum age of articles which may be downloaded from each feed. This
    # is specified in days - note fractions of days are allowed, for example,
    # 2.5 (2 and a half days). We could extend this to 2-5, but watch out ebook
    # creation time will increase as well. Setting this to large value will get
    # older articles as long as "max_articles_per_feed" remains set high.
    oldest_article = 1.5

    # The maximum number of articles which may be downloaded from each feed.
    # We've never seen more than about 25 articles in a single page.
    max_articles_per_feed = 50

    # Automatically extract all the text from downloaded article pages.
    # (Default: False)
    auto_cleanup = False

    # Ignore duplicates of articles that are present in more than one section.
    ignore_duplicate_articles = {"title", "url"}

    # Point to downloaded article instead of original web URL. (Default: False)
    resolve_internal_links = True

    # Keep only these tag(s).
    keep_only_tags = [classes("container")]

    # List of tags to removed.
    """Remove following classes and tags:
    Classes:
        adboxtop, adsizes: Remove advertisment box.
        audio-player-tts-sec: Remove "Listen to this article" widget.
        copyright: Remove copyright message.
        custom_read_button: Remove article suggestions.
        editor-share, ie-authorbox: Remove article's metadata like author,
                                    share button etc.
        h-text-widget: Remove special offers, subscriptions etc messages.
        ie-breadcrumb: Remove article's path from top.
        ie-first-publish: Remove publication date article's footer.
        ie-network-commenting: Remove comments article's footer.
        leftpanel: Remove everything after "leftpanel".
        livegif: Remove "Live Now" tag.
        more-from: Remove comments from article's bottom.
        news-guard: Remove NewsGuard banner from live blog.
        pdsc-related-modify:
        premium-story: Remove "Also Read" premium stories.
        story-premium: Remove "Premium" tag.
        story-rating: Rating is also present at the end of the article, so
                      remove rating from start of the article.
        storytags: Remove tags from article's footer.
        vernaculars-bar: Remove "Also Read in" section.
    """
    remove_tags = [
        classes(
            "adboxtop adsizes audio-player-tts-sec copyright custom_read_button editor-share h-text-widget ie-authorbox ie-breadcrumb ie-first-publish ie-network-commenting livegif more-from news-guard pdsc-related-modify premium-story story-premium story-rating storytags vernaculars-bar"
        ),
    ]
    remove_tags_after = [
        classes("leftpanel"),
    ]

    # Remove attributes.
    remove_attributes = ["height", "style", "width"]

    def parse_index(self):
        #    **** IMPORTANT USERS README ****
        #
        #  First select the feeds we want then scroll down below the feeds list
        #  and select the values we want for the other user preferences, like
        #  oldest_article etc.
        #
        #
        #  Select the feeds which we want in our ebook. Selected feed have no
        #  "#" at their beginning and de-selected feeds begin with a "#".
        #
        #  Eg.   ("Audio", "https://indianexpress.com/audio/" - include feed.
        #  Eg. # ("Audio", "https://indianexpress.com/audio/" - don't include feed.
        #
        # If E-book creation takes too long time, tweak "oldest_article",
        # "max_articles_per_feed", and (especially) "simultaneous_downloads"
        # settings.
        #
        # Select/de-select the feeds we want in our ebook.
        """
        # Note: "section" and "article" are interchangable in all URLs.

        # Section
        ("Daily Briefing", "https://indianexpress.com/section/live-news/"),
        ("India", "https://indianexpress.com/section/india/"),
        ("World", "https://indianexpress.com/section/world/"),

        ("Investigations", "https://indianexpress.com/section/express-exclusive/"),
        ("Explained", "https://indianexpress.com/section/explained/"),
        ("Research", "https://indianexpress.com/section/research/"),
        ("Sunday Eye", "https://indianexpress.com/section/express-sunday-eye/"),

        ("Delhi Confidential", "https://indianexpress.com/section/delhi-confidential/"),
        ("Elections", "https://indianexpress.com/elections/"),
        ("Politics", "https://indianexpress.com/section/political-pulse/"),

        ("Business", "https://indianexpress.com/section/business/"),

        ("40 Years Ago", "https://indianexpress.com/section/opinion/40-years-ago/"),
        ("Editorial", "https://indianexpress.com/section/opinion/editorials/"),
        ("Letters to Editor", "https://indianexpress.com/section/opinion/letters-to-editor/"),
        ("Opinion", "https://indianexpress.com/section/opinion/"),
        ("Interviews", "https://indianexpress.com/section/idea-exchange/"),
        ("Premium", "https://indianexpress.com/about/express-premium/"),

        ("Education", "https://indianexpress.com/section/education/"),
        ("UPSC", "https://indianexpress.com/section/upsc-current-affairs/"),
        ("Jobs", "https://indianexpress.com/section/jobs/"),

        ("Cities", "https://indianexpress.com/section/cities/"),

        ("Health", "https://indianexpress.com/section/lifestyle/health/"),
        ("Health Wellness", "https://indianexpress.com/section/health-wellness/"),
        ("Science", "https://indianexpress.com/section/technology/science/"),
        ("Tech", "https://indianexpress.com/section/technology/"),

        ("Books & Literature", "https://indianexpress.com/section/books-and-literature/"),
        ("Entertainment", "https://indianexpress.com/section/entertainment/"),
        ("Lifestyle", "https://indianexpress.com/section/lifestyle/"),
        ("Sports", "https://indianexpress.com/section/sports/"),
        # ("Latest News", "https://indianexpress.com/latest-news/"),
        # ("Latest News 2", "https://indianexpress.com/Latest.news/"),

        # ("Photos", "https://indianexpress.com/photos/"),
        # ("Videos", "https://indianexpress.com/videos/"),
        # ("Audio", "https://indianexpress.com/audio/"),
        ("Puzzles & Games", "https://indianexpress.com/section/puzzles-and-games/"),
        # ("Web Series", "https://indianexpress.com/web-stories/"),
        ("Trending", "https://indianexpress.com/section/trending/"),


        # Print
        ("Front Page", "https://indianexpress.com/print/front-page/"),
        ("Express Network", "https://indianexpress.com/print/express-network/),
        ("National Network", "https://indianexpress.com/print/national-network/"),
        ("Editorial", "https://indianexpress.com/print/the-editorial-page/"),
        ("Eye", "https://indianexpress.com/print/eye/"),
        ("Business", "https://indianexpress.com/print/business-print/"),
        ("Explained", "https://indianexpress.com/print/explained/")


        # E-Paper
        ("Today's Paper", "https://indianexpress.com/todays-paper/2023/07/20/"),


        # Et Al
        ("Express Et Al.", "https://indianexpress.com/et-al/"),
        """
        feeds_list = [
            ("Daily Briefing", "https://indianexpress.com/section/live-news/"),
            # ("India", "https://indianexpress.com/section/india/"),
            # ("World", "https://indianexpress.com/section/world/"),
            ("Investigations", "https://indianexpress.com/section/express-exclusive/"),
            ("Explained", "https://indianexpress.com/section/explained/"),
            ("Research", "https://indianexpress.com/section/research/"),
            ("Sunday Eye", "https://indianexpress.com/section/express-sunday-eye/"),
            (
                "Delhi Confidential",
                "https://indianexpress.com/section/delhi-confidential/",
            ),
            ("Elections", "https://indianexpress.com/elections/"),
            ("Politics", "https://indianexpress.com/section/political-pulse/"),
            # ("Business", "https://indianexpress.com/section/business/"),
            ("40 Years Ago", "https://indianexpress.com/section/opinion/40-years-ago/"),
            ("Editorial", "https://indianexpress.com/section/opinion/editorials/"),
            (
                "Letters to Editor",
                "https://indianexpress.com/section/opinion/letters-to-editor/",
            ),
            ("Opinion", "https://indianexpress.com/section/opinion/"),
            ("Interviews", "https://indianexpress.com/section/idea-exchange/"),
            ("Premium", "https://indianexpress.com/about/express-premium/"),
            ("Education", "https://indianexpress.com/section/education/"),
            # ("UPSC", "https://indianexpress.com/section/upsc-current-affairs/"),
            # ("Jobs", "https://indianexpress.com/section/jobs/"),
            # ("Cities", "https://indianexpress.com/section/cities/"),
            # ("Health", "https://indianexpress.com/section/lifestyle/health/"),
            # ("Health Wellness", "https://indianexpress.com/section/health-wellness/"),
            # ("Science", "https://indianexpress.com/section/technology/science/"),
            # ("Tech", "https://indianexpress.com/section/technology/"),
            # (
            #     "Books & Literature",
            #     "https://indianexpress.com/section/books-and-literature/",
            # ),
            # ("Entertainment", "https://indianexpress.com/section/entertainment/"),
            # ("Lifestyle", "https://indianexpress.com/section/lifestyle/"),
            # ("Sports", "https://indianexpress.com/section/sports/"),
            # ("Latest News", "https://indianexpress.com/latest-news/"),
            # ("Latest News 2", "https://indianexpress.com/Latest.news/"),
            # ("Photos", "https://indianexpress.com/photos/"),
            # ("Videos", "https://indianexpress.com/videos/"),
            # ("Audio", "https://indianexpress.com/audio/"),
            # ("Puzzles & Games", "https://indianexpress.com/section/puzzles-and-games/"),
            # ("Web Series", "https://indianexpress.com/web-stories/"),
            # ("Trending", "https://indianexpress.com/section/trending/"),
        ]

        feeds = []
        sections = dict()
        for section in feeds_list:
            section_title = section[0]
            section_url = section[1]
            soup = self.index_to_soup(section_url)
            articles = self.articles_from_page(soup)

            if articles is None:
                self.log(
                    f'Fix: No articles found from "{section_title}" section: {section_url}\n'
                )
                continue

            if len(articles) > 0:
                self.log("Section:", section_title)
                for article in articles:
                    self.log("\t", article["title"])

            # Add an extra newline for better readability of output log.
            self.log("")

            feeds.append((section_title, articles))

        return feeds

    def articles_from_page(self, soup):
        """Get list of all latest articles."""
        articles = []
        cnt = 0

        raw = str(soup)
        m = re.search(r'type="application/ld[+]json">[^<]+?"@type":"ItemList"', raw)
        if m is None:
            return None

        raw = raw[m.start() :]
        raw = raw.partition(">")[2]
        raw = raw.partition("<")[0]
        data = json.loads(raw)

        for article in data["itemListElement"]:
            title = article["name"]
            url = article["url"]
            desc = article["description"]

            # Get timestamp of the article.
            soup = self.index_to_soup(url)
            timestamp = soup.find("span", attrs={"itemprop": "dateModified"})

            if timestamp is None:
                self.log(
                    "Fix: No publication timestamp found on article's page. As likely other articles in the feed also don't contain timestamp, so ignoring whole feed...",
                    url,
                )
                return None

            date = timestamp["content"]
            date = parse_date(self.tag_to_string(date)).replace(tzinfo=None)
            today = datetime.now()

            # When first old article is found, don't check for other articles.
            # Articles are arranged such a way that they are already sorted
            # with respect to their publication date.
            if (today - date) > timedelta(self.oldest_article):
                self.log(
                    f"""Skipping all articles after, including "{title}" ({date}), as they are likely too old..."""
                )
                break

            articles.append({"title": title, "url": url, "desc": desc})

            cnt += 1
            if cnt == self.max_articles_per_feed:
                self.log(
                    f"Maximum number of article limit has been reached, i.e. {self.max_articles_per_feed}."
                )
                break

        return articles

    def preprocess_html(self, soup):
        # Remove tracking pixel.
        for img in soup.find_all(
            "img",
            attrs={"src": "https://data.indianexpress.com/election2019/track_1x1.jpg"},
        ):
            img.decompose()

        # Fix figure caption.
        for img in soup.find_all("img"):
            br = soup.new_tag("br")
            img.insert_after(br)

        # Embed authors.
        raw = str(soup)
        m = re.search(r'type="application/ld[+]json">[^<]+?"@type":"NewsArticle"', raw)
        raw = raw[m.start() :]
        raw = raw.partition(">")[2]
        raw = raw.partition("<")[0]
        data = json.loads(raw)

        authors = ""
        bylines = data.get("author")
        if bylines is not None:
            for i, byline in enumerate(bylines):
                if i == 0:
                    authors = f"""<a href='{byline["url"]}'>{byline["name"]}<a>"""
                    continue

                authors = "".join(
                    [
                        authors,
                        ", ",
                        f"""<a href='{byline["url"]}'>{byline["name"]}<a>""",
                    ]
                )

        authors = BeautifulSoup(authors, "html.parser")
        h5 = soup.new_tag("h5")
        h5.append(authors)
        desc = soup.find("h2")
        desc.insert_after(h5)

        return soup


calibre_most_common_ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"


# vim:fileencoding=utf-8 ft=python
